import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import routes from './routes';

import App from './App.vue';
import Menu from '@/components/Menu';
import Grid from '@/components/Grid';

import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.config.productionTip = false;

Vue.component('section-menu', Menu);
Vue.component('grid', Grid);

Vue.use(VueRouter);
Vue.use(VueAxios);
Vue.use(BootstrapVue);

const router = new VueRouter({
  mode: 'history',
  routes
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
});