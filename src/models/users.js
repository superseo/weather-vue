import axios from 'axios';

const Users = axios.create({
    baseURL: 'http://vuetask.kih.ru/api.php'
});

export default Users;