import axios from 'axios';

const Weather = axios.create({
    baseURL: 'http://api.openweathermap.org/data/2.5/weather',
    params: {
        appid: '271ae4441d5f7190865b421f84265e62',
        lang: 'ru',
    }
});

export default Weather;