import home from './pages/home.vue';
import weather from './pages/weather.vue';
import users from './pages/users.vue';
import stats from './pages/stats.vue';

export default [
  {
    path: '/',
    name: 'Home',
    component: home
  },
  {
    path: '/weather',
    name: 'Weather',
    component: weather
  },
  {
    path: '/users',
    name: 'Users',
    component: users
  },
  {
    path: '/stats',
    name: 'Stats',
    component: stats
  }
];